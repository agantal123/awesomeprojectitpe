import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, Text, Platform } from 'react-native';

export default class StatusBarScreen extends Component {
    render() {
        return (
            <View style={styles.MainContainer}>
                <StatusBar
                    barStyle="light-content"
                    hidden={false}
                    backgroundColor="#00BCD4"
                    translucent={true}
                    networkActivityIndicatorVisible={true}
                />
                <Text style={{ textAlign: 'center', fontSize: 25 }}> Notice the color of the Status bar, sir. </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        marginTop: (Platform.OS == 'ios') ? 20 : 0
    }
});
