import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from './screens/HomeScreen';
import DetailsScreen from './screens/DetailsScreen';
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen';
import Buttons from './screens/Buttons';
import DrawerLayoutAndroidscreen from './screens/DrawerLayoutAndroidscreen';
import FlatListscreen from './screens/FlatListscreen';
//import ImageScreen from './screens/ImageScreen';
import KeyboardAvoidingViewScreen from './screens/KeyboardAvoidingViewScreen';
import ListViewScreen from './screens/ListViewScreen';
import ModalScreen from './screens/ModalScreen';
import PickerScreen from './screens/PickerScreen';
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen';
import RefreshControlScreen from './screens/RefreshControlScreen';
import ScrollViewScreen from './screens/ScrollViewScreen';
import SectionListScreen from './screens/SectionListScreen';
import SliderScreen from './screens/SliderScreen';
import StatusBarScreen from './screens/StatusBarScreen';
import SwitchScreen from './screens/SwitchScreen';
import TextScreen from './screens/TextScreen';
import TextInputScreen from './screens/TextInputScreen';
import TouchableHighlightScreen from './screens/TouchableHighlightScreen';
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen';
import TouchableOpacityScreen from './screens/TouchableOpacityScreen';
import TouchableWithoutFeedbackScreen from './screens/TouchableWithoutFeedbackScreen';
import ViewPagerAndroidScreen from './screens/ViewPagerAndroidScreen';
import ViewScreen from './screens/ViewScreen';
import WebViewScreen from './screens/WebViewScreen';


const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    ActivityIndicator: {
      screen: ActivityIndicatorScreen,
    },
    Buttons: {
      screen: Buttons,
    },
    DrawerLayoutAndroid: {
      screen: DrawerLayoutAndroidscreen,
    },
    FlatList: {
      screen: FlatListscreen,
    },
  /*  Image: {
      screen: ImageScreen,
    },
  */
    KeyboardAvoidingView: {
      screen: KeyboardAvoidingViewScreen,
    },
    ListView: {
      screen: ListViewScreen,
    },
    Modal: {
      screen: ModalScreen,
    },
    Picker: {
      screen: PickerScreen,
    },
    ProgressBarAndroid: {
      screen: ProgressBarAndroidScreen,
    },
    RefreshControl: {
      screen: RefreshControlScreen,
    },
    ScrollView: {
      screen: ScrollViewScreen,
    },
    SectionList: {
      screen: SectionListScreen,
    },
    Slider: {
      screen: SliderScreen,
    },
    StatusBar: {
      screen: StatusBarScreen,
    },
    Switch: {
      screen: SwitchScreen,
    },
    Text: {
      screen: TextScreen,
    },
    TextInput: {
      screen: TextInputScreen,
    },
    TouchableHighlight: {
      screen: TouchableHighlightScreen,
    },
    TouchableNativeFeedback: {
      screen: TouchableNativeFeedbackScreen,
    },
    TouchableOpacity: {
      screen: TouchableOpacityScreen,
    },
    TouchableWithoutFeedback: {
      screen: TouchableWithoutFeedbackScreen,
    },
    ViewPagerAndroidScreen: {
      screen: ViewPagerAndroidScreen,
    },
    View: {
      screen: ViewScreen,
    },
    WebView: {
      screen: WebViewScreen,
    },




  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends Component {
  render() {
    return <RootStack />
  }
}
